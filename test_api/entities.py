from typing import Dict, NamedTuple, Optional, Union
from uuid import UUID, uuid4


class Entity(NamedTuple):
    name: str
    text: str


STORAGE: Dict[str, Entity] = {}

Uuid = Union[UUID, str]


def find(uuid: Uuid) -> Optional[Entity]:
    """Find entity by UUID"""
    return STORAGE.get(str(uuid), None)


def find_by_name_text(name) -> Optional[Entity]:
    for ent in STORAGE.values():
        if ent.name == name:
            return ent
    return None


def create(name: str, text: str) -> str:
    """Add entity to storage"""
    new_uuid = str(uuid4())
    STORAGE[new_uuid] = Entity(name, text)
    return new_uuid


def delete(uuid: Uuid):
    STORAGE.pop(str(uuid))


def update(uuid: Uuid, name: str = None, text: str = None):
    """Update existing entry"""
    old_entry = STORAGE[str(uuid)]
    if name is None:
        name = old_entry.name
    if text is None:
        text = old_entry.text
    new_entity = Entity(name, text)
    STORAGE[uuid] = new_entity
