from flask import Flask, Response, send_file
from flask_restful import Api, Resource, reqparse
from ocomone import Resources
from werkzeug.exceptions import BadRequest, NotFound

from .entities import create, delete, find, update

APP = Flask("API")
API = Api(APP)

NOT_POST = ["GET", "PUT", "PATCH", "DELETE"]

SPEC = Resources(__file__, "../spec")


@APP.route("/")
def spec():
    """Render swagger specification"""
    return send_file(SPEC["index.html"])


@APP.route("/api.yml")
def yml():
    return send_file(SPEC["api.yml"])


_body_parser = reqparse.RequestParser()
_body_parser.add_argument("name")
_body_parser.add_argument("text")


# noinspection PyMethodMayBeStatic
class Entity(Resource):
    """Entity operations"""

    # pylint: disable=no-self-use

    def get(self, uuid):
        """Get existing entity"""
        result = find(uuid)
        if result is None:
            raise NotFound
        return result._asdict()

    def delete(self, uuid):
        """Remove existing entity"""
        try:
            delete(uuid)
            return Response(status=204)
        except KeyError:
            raise NotFound

    def _update(self, uuid, rewrite: bool):
        request_data = _body_parser.parse_args(strict=True)
        if find(uuid) is None:
            raise NotFound
        check_method = any if rewrite else all
        if check_method([request_data.name is None, request_data.text is None]):
            raise BadRequest("Invalid JSON data")

        # following can trigger exception, do you know why?
        update(uuid, request_data["name"], request_data["text"])
        return {}, 204

    def put(self, uuid):
        """Rewrite existing entity"""
        return self._update(uuid, True)

    def patch(self, uuid):
        """Update field of existing entity"""
        return self._update(uuid, False)

    def post(self):
        """Create new entity"""
        request_data = _body_parser.parse_args()
        if ("text" not in request_data) and ("name" not in request_data):
            raise BadRequest("Invalid JSON data")
        uuid = create(request_data["name"], request_data["text"])
        return {}, 201, {"Location": f"/entity/{uuid}"}


API.add_resource(Entity, "/entity/<uuid:uuid>", "/entity")
