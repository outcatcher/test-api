import random
import string
import time
from threading import Thread

import pytest
from ocomone import BaseUrlSession
from wsgiserver import WSGIServer

from test_api import APP

ENTITY_URL = "/entity"


def _rand_str():
    return "".join(random.choice(string.ascii_letters) for _ in range(10))


@pytest.fixture
def random_name():
    return _rand_str()


@pytest.fixture
def random_text():
    return _rand_str()


def __delete(_session: BaseUrlSession, uuid):
    _session.delete(f"{ENTITY_URL}/{uuid}")


@pytest.fixture
def entity_url(session, created_url) -> str:
    yield created_url
    session.delete(created_url)


@pytest.fixture
def created_url(session, random_name, random_text) -> str:
    data = {"name": random_name, "text": random_text}
    location: str = session.post(ENTITY_URL, json=data).headers["Location"]
    return location


@pytest.fixture(scope="session")
def session() -> BaseUrlSession:
    port = 5014
    Thread(target=WSGIServer(APP, port=port).start, daemon=True).start()
    session = BaseUrlSession(f"http://localhost:{port}")
    session.trust_env = False
    while session.get("").status_code != 200:  # wait until server is up and running
        time.sleep(0.1)
    yield session
    session.close()
