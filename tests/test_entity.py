import uuid

import pytest

from test_api.entities import Entity
from tests.conftest import ENTITY_URL


def test_create(random_name, random_text, session):
    resp = session.post(ENTITY_URL, json={"name": random_name, "text": random_text})
    assert resp.status_code == 201
    location: str = resp.headers["Location"]

    assert "/entity/" in location
    _uuid = location.rsplit("/", 1)[1]

    assert str(uuid.UUID(_uuid)) == _uuid  # raises exception if uuid is invalid or has invalid format


def test_remove(session, created_url):
    resp = session.delete(created_url)
    assert resp.status_code == 204
    find = session.get(created_url)
    assert find.status_code == 404


def test_find(random_name, random_text, entity_url, session):
    response = session.get(entity_url)
    assert response.status_code == 200
    found = response.json()
    found = Entity(**found)
    assert found.name == random_name
    assert found.text == random_text


__UPD_DATA = {"text": "updated_t", "name": "updated_n"}


@pytest.mark.parametrize(("method", "data"),
                         [
                             ("GET", None),
                             ("DELETE", None),
                             ("PUT", __UPD_DATA),
                             ("PATCH", __UPD_DATA),
                         ])
def test_404(session, method, data):
    random_uuid = str(uuid.uuid4())
    response = session.request(method, f"{ENTITY_URL}/{random_uuid}", json=data)
    assert response.status_code == 404


@pytest.mark.parametrize(("method", "data"),
                         [
                             ("PUT", {"name": "asd"}),
                             ("PUT", {"text": "asd"}),
                             ("PUT", {"other data": "sd"}),
                             ("PUT", {}),
                             ("PATCH", {}),
                             ("PATCH", {"other data": "sd"}),
                         ])
def test_400(session, method, data, created_url):
    response = session.request(method, created_url, json=data)
    assert response.status_code == 400
