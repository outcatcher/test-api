# Test automation interview

Here be ~~dragons~~ a few tasks for test automation engineer interview

## Child :baby:

Let's check some basic stuff

1. `FizzBuzz` game: 
    1. Numbers `-100..100`
    1. For numbers that are multiples of 3 print `"Fizz"`
    1. For numbers that are multiples of 5 print `"Buzz"`
    1. For numbers that are multiples of 3 and 5 print `"FizzBuzz"`
    1. Otherwise print number

1. `Counting The Longest` game:
    1. Some random string with repeated characters (like `aaacaaccbbbcccabccbbbb`)
    1. For each character print size of the longest sequence of this character, e.g. `a: 3, b: 4`

## Teen :girl: :boy: 

### _Some Python_

* What does the following code do?
* How else can it be written?

```python
def some_function(resp, network_id):
    if next(iter(resp), None) is None:
        raise RuntimeError('No Subnet Exists')
    else:
        for subnet in resp:
            if subnet['network_id'] == network_id:
                return subnet['id']
```

### _Here goes testing!_

Write API tests using documentation from `spec/`

### _Debugging magic_

App on remote host stop responding to requests :mag_right: let's find the issue (without real server so far :unamused:)



## Adult :man: :woman:

_For serious guys_

Write API using documentation from `spec/`
